# Public Test Project

Use this project to test gitlab public api, e.g., for changelog_helper.

## Notes

- Create issues and merge requests that you want changelog_helper to report
- Merge the changes into branch `main`, HOWEVER
- Do not push any changes to branch `prod`
- This way, when changelog_helper compares `prod` to `main` all of the merged issues/merge requests will(may) be reported
